import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styles: []
})
export class CoursesComponent implements OnInit {

  public courses: any;

  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.courses = await this._context.model('students/me/courses')
                        .take(5)
                        .expand('course')
                        .orderByDescending('dateModified')
                        .getItems();
  }

}
