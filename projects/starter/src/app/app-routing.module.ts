import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FullLayoutComponent} from './layouts/full-layout.component';
import {HomeComponent} from './home/home.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AuthModule } from '@universis/common';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: '',
    component: FullLayoutComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      }
      ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    AuthModule
    ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
