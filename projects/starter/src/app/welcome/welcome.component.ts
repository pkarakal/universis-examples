import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [ './welcome.component.scss' ]
})
export class WelcomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
   
  }

}
