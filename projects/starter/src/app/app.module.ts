import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AuthModule, ErrorModule, SharedModule, ConfigurationService } from '@universis/common';
import { MostModule, AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { AppRoutingModule } from './app-routing.module';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {TranslateModule} from '@ngx-translate/core';
import { WelcomeComponent } from './welcome/welcome.component';
import { CoursesComponent } from './courses/courses.component';
import { ProfileComponent } from './profile/profile.component';
import { AverageComponent } from './average/average.component';

@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    HomeComponent,
    WelcomeComponent,
    CoursesComponent,
    ProfileComponent,
    AverageComponent
  ],
  imports: [
    BrowserModule,
    TranslateModule.forRoot(),
    MostModule,
    SharedModule,
    AuthModule,
    ErrorModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
      {
            provide: DATA_CONTEXT_CONFIG, useValue: {
                base: '/',
                options: {
                    useMediaTypeExtensions: false,
                    useResponseConversion: true
                }
            }
        },
        // use has location strategy
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        },
        // provide configuration service
        {
            provide: APP_INITIALIZER,
            // use APP_INITIALIZER to load application configuration
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                // load application configuration
                    return configurationService.load();
                },
            deps: [ ConfigurationService ],
            multi: true
        },
        AngularDataContext
    ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
