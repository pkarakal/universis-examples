import { Component, OnInit } from '@angular/core';
import { UserService } from '@universis/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private _userService: UserService, private _router: Router) { }

  ngOnInit() {
    this._userService.getUser().then( user => {
      if (user == null) {
        return this._router.navigate(['/welcome'], { skipLocationChange: true });
      }
    });
  }

}
